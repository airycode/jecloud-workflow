/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.exception;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.je.bpm.engine.ActivitiObjectNotFoundException;
import com.je.common.base.result.BaseRespResult;
import com.je.servicecomb.exception.CloudExceptionExceptionToProducerResponseConverter;
import com.je.servicecomb.exception.CloudExceptionInfoStorage;
import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.swagger.invocation.Response;
import org.apache.servicecomb.swagger.invocation.SwaggerInvocation;
import org.apache.servicecomb.swagger.invocation.exception.ExceptionToProducerResponseConverter;
import org.apache.servicecomb.swagger.invocation.exception.InvocationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @program: jecloud-workflow
 * @author: LIULJ
 * @create: 2021/7/9
 * @description:
 */
public class ActivitiObjectNotFoundExceptionConverter implements ExceptionToProducerResponseConverter<ActivitiObjectNotFoundException> {

    private static final Logger logger = LoggerFactory.getLogger(CloudExceptionExceptionToProducerResponseConverter.class);

    private static final String ERROR_CODE = "9999";

    /**
     * 异常处理顺序
     */
    private static final int ORDER = 1;

    @Override
    public Class<ActivitiObjectNotFoundException> getExceptionClass() {
        return ActivitiObjectNotFoundException.class;
    }

    @Override
    public Response convert(SwaggerInvocation swaggerInvocation, ActivitiObjectNotFoundException e) {
        logger.error(ExceptionUtil.getMessage(e));
        String url = ((Invocation) swaggerInvocation).getSchemaMeta().getSwagger().getBasePath();
        logger.info("throw PlatformException error...........url....." + url);
        logger.info("throw PlatformException error...........context....." + ((Invocation) swaggerInvocation).getContext().toString());
        BaseRespResult baseRespResult = BaseRespResult.errorResult("9999", "", "当前任务或流程实例已结束，请重新进入表单查看！");
        logger.error("log.isErrorEnabled():{}", logger.isErrorEnabled());
        logger.info("log.isInfoEnabled():{}", logger.isInfoEnabled());
        CloudExceptionInfoStorage.saveExceptionLog(url,((Invocation) swaggerInvocation).getRequestEx().getParameterMap(),baseRespResult.getMessage(),baseRespResult.getCode(),e.getStackTrace());
        e.printStackTrace();
        return Response.succResp(baseRespResult);
    }

    @Override
    public int getOrder() {
        return ORDER;
    }
}
