/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.usertask.impl;

import com.google.common.base.Strings;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.workflow.service.usertask.CurrentUserParam;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 已办理
 */
@Service(value = "upcomingApprovedService")
public class CurrentUserApprovedServiceImpl extends AbstractCurrentUserService {

    @Override
    public Map<String, Object> getTask(CurrentUserParam upcomingParam) {
        ConditionsWrapper conditionsWrapper = buildConditionsWrapper(upcomingParam);
        if (Strings.isNullOrEmpty(upcomingParam.getEnd()) || upcomingParam.getEnd().equals("0")) {
            return getRunTask(conditionsWrapper, upcomingParam);
        } else {
            return getEndTask(conditionsWrapper, upcomingParam);
        }
    }

    @Override
    public Long getBadge(CurrentUserParam upcomingParam) {
        ConditionsWrapper conditionsWrapper = buildConditionsWrapper(upcomingParam);
        buildRunApprovedWrapper(conditionsWrapper);
        return abstractGetRunTaskBadge("je_workflow_v_rn_task", conditionsWrapper);
    }

    private Map<String, Object> getRunTask(ConditionsWrapper conditionsWrapper, CurrentUserParam upcomingParam) {
        buildRunApprovedWrapper(conditionsWrapper);
        return abstractGetRunTask("je_workflow_v_rn_task", conditionsWrapper, upcomingParam.getPage());
    }

    private void buildRunApprovedWrapper(ConditionsWrapper conditionsWrapper) {
        String loginUserId = Authentication.getAuthenticatedUser().getDeptId();
        //是否已办理
        conditionsWrapper.eq("TASK_HANDLE", "1");
        //已办理去除发起人是我
        conditionsWrapper.ne("EXECUTION_STARTER", loginUserId);
        //办理人里面有我
        conditionsWrapper.eq("ASSIGNEE_ID", Authentication.getAuthenticatedUser().getDeptId());
        conditionsWrapper.groupBy("EXECUTION_PIID");
    }

    private Map<String, Object> getEndTask(ConditionsWrapper conditionsWrapper, CurrentUserParam upcomingParam) {
        buildEndApprovedWrapper(conditionsWrapper);
        return abstractGetEndTask(conditionsWrapper, upcomingParam.getPage());
    }

    private void buildEndApprovedWrapper(ConditionsWrapper conditionsWrapper) {
        conditionsWrapper.eq("ASSIGNEE_ID", Authentication.getAuthenticatedUser().getDeptId());
        conditionsWrapper.ne("EXECUTION_STARTER", Authentication.getAuthenticatedUser().getDeptId());
        conditionsWrapper.groupBy("EXECUTION_PIID");
        conditionsWrapper.orderByDesc("COLLECT_SY_CREATETIME", "SY_CREATETIME");
    }


}
