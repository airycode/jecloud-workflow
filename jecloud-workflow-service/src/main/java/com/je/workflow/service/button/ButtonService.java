/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.button;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface ButtonService {

    List<Map<String, Object>> getButtons(String prod, String tableCode, String funcCode, String beanId);

    List<String> getParams(String operationId);

    /**
     * @param operationId
     * @param request
     * @return
     */
    Map<String, Object> buildOperationCustomerParam(String operationId, HttpServletRequest request);

    /**
     * 指定按钮命令
     *
     * @param operationId            命令id
     * @param prod                   产品id
     * @param funcCode               功能code
     * @param pdid                   流程部署id
     * @param operationCustomerParam 自定义参数
     * @return
     */
    Map<String, Object> operate(String operationId, String prod, String funcCode, String pdid, String beanId, String tableCode,String funcId,
                                      Map<String, Object> operationCustomerParam);

    /**
     * 添加常用人员信息
     *
     * @param assignee
     */
    void addCommonUser(String assignee);
}
