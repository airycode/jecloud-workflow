/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.service.usertask;

import com.je.ibatis.extension.plugins.pagination.Page;

public class CurrentUserParam {

    String userName;

    String startTime;

    String endTime;

    String end;

    Page page;

    CurrentUserTaskEnum findCurrentTaskEnum;

    String sort;

    public static CurrentUserParam build(String userName, String startTime, String endTime, String end, Page page, CurrentUserTaskEnum findCurrentTaskEnum, String sort) {
        CurrentUserParam upcomingParam = new CurrentUserParam();
        upcomingParam.setEnd(end);
        upcomingParam.setEndTime(endTime);
        upcomingParam.setStartTime(startTime);
        upcomingParam.setUserName(userName);
        upcomingParam.setFindCurrentTaskEnum(findCurrentTaskEnum);
        if (page == null) {
            page = new Page(0, 10);
        }
        upcomingParam.setPage(page);
        upcomingParam.setSort(sort);
        return upcomingParam;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public CurrentUserTaskEnum getFindCurrentTaskEnum() {
        return findCurrentTaskEnum;
    }

    public void setFindCurrentTaskEnum(CurrentUserTaskEnum findCurrentTaskEnum) {
        this.findCurrentTaskEnum = findCurrentTaskEnum;
    }
}
