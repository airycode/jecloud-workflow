/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.config;

import com.je.auth.AuthLoginManager;
import com.je.auth.AuthTempInterface;
import com.je.auth.AuthTokenDao;
import com.je.auth.RedisAuthTokenDao;
import com.je.auth.check.RbacInterface;
import com.je.auth.impl.AuthTempDefaultImpl;
import com.je.auth.impl.DefaultAuthLoginManagerImpl;
import com.je.rbac.service.RbacInterfaceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

@Configuration
public class AuthConfig {

    @Bean
    public AuthTokenDao authTokenDao(StringRedisTemplate stringRedisTemplate, RedisTemplate<String, Object> redisTemplate) {
        return new RedisAuthTokenDao(stringRedisTemplate, redisTemplate);
    }

    @Bean
    public RbacInterface rbacInterface() {
        return new RbacInterfaceImpl();
    }

    @Bean
    public AuthTempInterface authTempInterface() {
        return new AuthTempDefaultImpl();
    }
    
    @Bean
    public AuthLoginManager authLoginManager() {
        return new DefaultAuthLoginManagerImpl();
    }

}
