/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.config;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.google.common.base.Strings;
import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * xxl-job config
 */
@Configuration
public class XxlJobConfig {

    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        Config config = ConfigService.getConfig("xxljob");
        XxlJobSpringExecutor xxlJobSpringExecutor = createExecutor(config);
        return xxlJobSpringExecutor;
    }

    protected XxlJobSpringExecutor createExecutor(Config config){
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(config.getProperty("xxl.job.admin.addresses",""));
        xxlJobSpringExecutor.setAppname(config.getProperty("xxl.job.executor.appname",""));
        xxlJobSpringExecutor.setAddress(config.getProperty("xxl.job.executor.address",""));
        if (!Strings.isNullOrEmpty(config.getProperty("xxl.job.executor.ip",""))) {
            xxlJobSpringExecutor.setIp(config.getProperty("xxl.job.executor.ip",""));
        }
        if (!Strings.isNullOrEmpty(config.getProperty("xxl.job.executor.port",""))) {
            xxlJobSpringExecutor.setPort(config.getIntProperty("xxl.job.executor.port",0));
        }
        xxlJobSpringExecutor.setAccessToken(config.getProperty("xxl.job.accessToken",""));
        if (!Strings.isNullOrEmpty(config.getProperty("xxl.job.executor.logpath",""))) {
            xxlJobSpringExecutor.setLogPath(config.getProperty("xxl.job.executor.logpath",""));
        }
        if (!Strings.isNullOrEmpty(config.getProperty("xxl.job.executor.logretentiondays",""))) {
            xxlJobSpringExecutor.setLogRetentionDays(config.getIntProperty("xxl.job.executor.logretentiondays",0));
        }
        return xxlJobSpringExecutor;
    }

}