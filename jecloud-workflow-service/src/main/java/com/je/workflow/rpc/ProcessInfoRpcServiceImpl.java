/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.rpc;

import com.google.common.base.Strings;
import com.je.bpm.engine.impl.identity.Authentication;
import com.je.bpm.model.process.model.ProcessNextNodeInfo;
import com.je.common.auth.impl.RealOrganization;
import com.je.common.auth.impl.RealOrganizationUser;
import com.je.common.auth.impl.account.Account;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.util.SecurityUserHolder;
import com.je.workflow.model.*;
import com.je.workflow.rpc.workflow.ProcessInfoRpcService;
import com.je.workflow.service.button.ButtonService;
import com.je.workflow.service.usertask.ProcessInfoService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RpcSchema(schemaId = "processInfoRpcService")
public class ProcessInfoRpcServiceImpl implements ProcessInfoRpcService {

    @Autowired
    private ButtonService buttonService;
    @Autowired
    private ProcessInfoService processInfoService;

    @Override
    public List<Map<String, Object>> getButtons(ButtonsVo buttonsVo) {
        if (Authentication.getAuthenticatedUser() == null) {
            //设置当前登录人
            Account account = SecurityUserHolder.getCurrentAccount();
            if (account == null) {
                AccountVo accountVo = buttonsVo.getAccount();
                if (accountVo != null) {
                    account = new Account();
                    account.setId(accountVo.getDeptId());
                    account.setDeptId(accountVo.getDeptId());
                    account.setCode(accountVo.getCode());
                    account.setName(accountVo.getName());
                    RealOrganizationUser realUser = new RealOrganizationUser();
                    //用户真实id
                    realUser.setId(accountVo.getRealUserId());
                    realUser.setCode(accountVo.getCode());
                    realUser.setName(accountVo.getName());
                    realUser.setPhone(accountVo.getPhone());
                    realUser.setEmail(accountVo.getEmail());

                    //部门信息
                    if(!Strings.isNullOrEmpty(accountVo.getDepartmentId())&&!Strings.isNullOrEmpty(accountVo.getDepartmentName())){
                        RealOrganization department = new RealOrganization();
                        department.setId(accountVo.getDepartmentId());
                        department.setCode(accountVo.getDepartmentCode());
                        department.setName(accountVo.getDepartmentName());
                        realUser.setOrganization(department);
                    }
                    account.setRealUser(realUser);
                }
            }
            if (account == null) {
                throw new PlatformException("当前人员信息为空！", PlatformExceptionEnum.UNKOWN_ERROR);
            }
            SecurityUserHolder.put(account);
            Authentication.setAuthenticatedUser(account);
        }

        return buttonService.getButtons(buttonsVo.getProd(), buttonsVo.getTableCode(), buttonsVo.getFuncCode(),
                buttonsVo.getBeanId());
    }

    @Override
    public List<ProcessNextNodeInfoVo> getSubmitOutGoingNode(SubmitOutGoingNodeVo submitOutGoingNodeVo) {
        if (Authentication.getAuthenticatedUser() == null) {
            //设置当前登录人
            Account account = SecurityUserHolder.getCurrentAccount();
            if (account == null) {
                AccountVo accountVo = submitOutGoingNodeVo.getAccount();
                if (accountVo != null) {
                    account = new Account();
                    account.setId(accountVo.getDeptId());
                    account.setDeptId(accountVo.getDeptId());
                    account.setCode(accountVo.getCode());
                    account.setName(accountVo.getName());
                    RealOrganizationUser realUser = new RealOrganizationUser();
                    //用户真实id
                    realUser.setId(accountVo.getRealUserId());
                    realUser.setCode(accountVo.getCode());
                    realUser.setName(accountVo.getName());
                    realUser.setPhone(accountVo.getPhone());
                    realUser.setEmail(accountVo.getEmail());
                    //部门信息
                    if(!Strings.isNullOrEmpty(accountVo.getDepartmentId())&&!Strings.isNullOrEmpty(accountVo.getDepartmentName())){
                        RealOrganization department = new RealOrganization();
                        department.setId(accountVo.getDepartmentId());
                        department.setCode(accountVo.getDepartmentCode());
                        department.setName(accountVo.getDepartmentName());
                        realUser.setOrganization(department);
                    }
                    account.setRealUser(realUser);
                }
            }
            if (account == null) {
                throw new PlatformException("当前人员信息为空！", PlatformExceptionEnum.UNKOWN_ERROR);
            }
            SecurityUserHolder.put(account);
            Authentication.setAuthenticatedUser(account);
        }

        List<ProcessNextNodeInfo> submitOutGoingNode = processInfoService.getSubmitOutGoingNode(submitOutGoingNodeVo.getTaskId(), submitOutGoingNodeVo.getPdid(),
                submitOutGoingNodeVo.getProd(), submitOutGoingNodeVo.getBeanId(), submitOutGoingNodeVo.getTableCode());
        List<ProcessNextNodeInfoVo> processNextNodeInfoVoList = new ArrayList<>();
        for (ProcessNextNodeInfo processNextNodeInfo : submitOutGoingNode) {
            ProcessNextNodeInfoVo nodeInfoVoList = new ProcessNextNodeInfoVo();
            nodeInfoVoList.setNodeName(processNextNodeInfo.getNodeName());
            nodeInfoVoList.setNodeId(processNextNodeInfo.getNodeId());
            nodeInfoVoList.setSubmitDirectly(processNextNodeInfo.getSubmitDirectly());
            nodeInfoVoList.setType(processNextNodeInfo.getType());
            nodeInfoVoList.setTarget(processNextNodeInfo.getTarget());
            nodeInfoVoList.setIsJump(processNextNodeInfo.getIsJump());
            processNextNodeInfoVoList.add(nodeInfoVoList);
        }
        return processNextNodeInfoVoList;
    }

    @Override
    public Map<String, Object> operate(ButtonOperateVo buttonOperateVo) {
        if (Authentication.getAuthenticatedUser() == null) {
            //设置当前登录人
            Account account = SecurityUserHolder.getCurrentAccount();
            if (account == null) {
                AccountVo accountVo = buttonOperateVo.getAccount();
                if (accountVo != null) {
                    account = new Account();
                    account.setId(accountVo.getDeptId());
                    account.setDeptId(accountVo.getDeptId());
                    account.setCode(accountVo.getCode());
                    account.setName(accountVo.getName());
                    RealOrganizationUser realUser = new RealOrganizationUser();
                    //用户真实id
                    realUser.setId(accountVo.getRealUserId());
                    realUser.setCode(accountVo.getCode());
                    realUser.setName(accountVo.getName());
                    realUser.setPhone(accountVo.getPhone());
                    realUser.setEmail(accountVo.getEmail());
                    //部门信息
                    if(!Strings.isNullOrEmpty(accountVo.getDepartmentId())&&!Strings.isNullOrEmpty(accountVo.getDepartmentName())){
                        RealOrganization department = new RealOrganization();
                        department.setId(accountVo.getDepartmentId());
                        department.setCode(accountVo.getDepartmentCode());
                        department.setName(accountVo.getDepartmentName());
                        realUser.setOrganization(department);
                    }
                    account.setRealUser(realUser);
                }
            }
            if (account == null) {
                throw new PlatformException("当前人员信息为空！", PlatformExceptionEnum.UNKOWN_ERROR);
            }
            SecurityUserHolder.put(account);
            Authentication.setAuthenticatedUser(account);
        }

        return buttonService.operate(buttonOperateVo.getOperationId(), buttonOperateVo.getProd(), buttonOperateVo.getFuncCode(),
                buttonOperateVo.getPdid(), buttonOperateVo.getBeanId(), buttonOperateVo.getTableCode(), buttonOperateVo.getFuncId(),
                buttonOperateVo.getOperationCustomerParam());
    }
}
