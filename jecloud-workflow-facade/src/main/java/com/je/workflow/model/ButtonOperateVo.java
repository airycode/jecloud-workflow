/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.workflow.model;

import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ButtonOperateVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String operationId;
    private String prod;
    private String funcCode;
    private String pdid;
    private String beanId;
    private String tableCode;
    private String funcId;
    private Map<String, Object> operationCustomerParam = new HashMap<String, Object>();
    private AccountVo account;

    private ButtonOperateVo() {
    }

    private ButtonOperateVo(Builder builder) {
        this.operationId = builder.operationId;
        this.prod = builder.prod;
        this.funcCode = builder.funcCode;
        this.pdid = builder.pdid;
        this.beanId = builder.beanId;
        this.tableCode = builder.tableCode;
        this.funcId = builder.funcId;
        this.operationCustomerParam = builder.operationCustomerParam;
        this.account = builder.account;
    }

    public static class Builder {
        private String operationId;
        private String prod;
        private String funcCode;
        private String pdid;
        private String beanId;
        private String tableCode;
        private String funcId;
        private Map<String, Object> operationCustomerParam = new HashMap<String, Object>();
        private AccountVo account;

        public Builder() {
        }

        public ButtonOperateVo build() {
            if (Strings.isNullOrEmpty(operationId)) {
                throw new IllegalArgumentException("参数 operationId 不可以为空");
            }
            if (Strings.isNullOrEmpty(prod)) {
                throw new IllegalArgumentException("参数 prod 不可以为空");
            }
            if (Strings.isNullOrEmpty(funcCode)) {
                throw new IllegalArgumentException("参数 funcCode 不可以为空");
            }
            if (Strings.isNullOrEmpty(pdid)) {
                throw new IllegalArgumentException("参数 pdid 不可以为空");
            }
            if (Strings.isNullOrEmpty(beanId)) {
                throw new IllegalArgumentException("参数 beanId 不可以为空");
            }
            if (Strings.isNullOrEmpty(tableCode)) {
                throw new IllegalArgumentException("参数 tableCode 不可以为空");
            }
            if (operationCustomerParam.get("assignee") == null || Strings.isNullOrEmpty(operationCustomerParam.get("assignee").toString())) {
                throw new IllegalArgumentException("参数 operationCustomerParam 中 assignee 不可以为空");
            }
            if (operationCustomerParam.get("comment") == null || Strings.isNullOrEmpty(operationCustomerParam.get("comment").toString())) {
                throw new IllegalArgumentException("参数 operationCustomerParam 中 comment 不可以为空");
            }
            if (Objects.isNull(account)) {
                throw new IllegalArgumentException("参数 account 不可以为空");
            }
            return new ButtonOperateVo(this);
        }

        public Builder operationId(String operationId) {
            if (Strings.isNullOrEmpty(operationId)) {
                throw new IllegalArgumentException("参数operationId不可以为空");
            }
            this.operationId = operationId;
            return this;
        }

        public Builder prod(String prod) {
            if (Strings.isNullOrEmpty(prod)) {
                throw new IllegalArgumentException("参数prod不可以为空");
            }
            this.prod = prod;
            return this;
        }

        public Builder funcCode(String funcCode) {
            if (Strings.isNullOrEmpty(funcCode)) {
                throw new IllegalArgumentException("参数 funcCode 不可以为空");
            }
            this.funcCode = funcCode;
            return this;
        }

        public Builder pdid(String pdid) {
            if (Strings.isNullOrEmpty(pdid)) {
                throw new IllegalArgumentException("参数pdid不可以为空");
            }
            this.pdid = pdid;
            return this;
        }

        public Builder beanId(String beanId) {
            if (Strings.isNullOrEmpty(beanId)) {
                throw new IllegalArgumentException("参数beanId不可以为空");
            }
            this.beanId = beanId;
            return this;
        }

        public Builder tableCode(String tableCode) {
            if (Strings.isNullOrEmpty(tableCode)) {
                throw new IllegalArgumentException("参数tableCode不可以为空");
            }
            this.tableCode = tableCode;
            return this;
        }

        public Builder funcId(String funcId) {
            this.funcId = funcId;
            return this;
        }

        public Builder operationCustomerParam(Map<String, Object> operationCustomerParam) {
            if (operationCustomerParam.get("assignee") == null || Strings.isNullOrEmpty(operationCustomerParam.get("assignee").toString())) {
                throw new IllegalArgumentException("参数 operationCustomerParam 中 assignee 不可以为空");
            }
            if (operationCustomerParam.get("comment") == null || Strings.isNullOrEmpty(operationCustomerParam.get("comment").toString())) {
                throw new IllegalArgumentException("参数 operationCustomerParam 中 comment 不可以为空");
            }
            if (operationCustomerParam.get("target") == null || Strings.isNullOrEmpty(operationCustomerParam.get("target").toString())) {
                throw new IllegalArgumentException("参数 operationCustomerParam 中 target 不可以为空");
            }


            this.operationCustomerParam = operationCustomerParam;
            return this;
        }

        public Builder account(AccountVo account) {
            this.account = account;
            return this;
        }
    }

    public String getOperationId() {
        return operationId;
    }

    public String getProd() {
        return prod;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public String getPdid() {
        return pdid;
    }

    public String getBeanId() {
        return beanId;
    }

    public String getTableCode() {
        return tableCode;
    }

    public String getFuncId() {
        return funcId;
    }

    public Map<String, Object> getOperationCustomerParam() {
        return operationCustomerParam;
    }

    public AccountVo getAccount() {
        return account;
    }
}
